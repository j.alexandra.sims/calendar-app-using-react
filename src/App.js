
import React, { Component } from 'react';
import icon from './user_icon_white.png';
import Menu from './components/Menu';
import EventsList from './components/EventsList';
import Month from './components/Month';

// import DBfunctions from './components/DBfunctions'

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      fullDate: 0,
      day: 0,
      month: 0,
      year: 0
    };
    // console.log("Day-Constructor: " + this.state.day);
  }

  setCurrentDay(passedInDay) {
    this.setState({
      day: passedInDay
    });
  }

  setCurrentMonth(passedInMonth) {
    this.setState({
      month: passedInMonth
    })
  }

  setCurrentYear(passedInYear) {
    this.setState({
      year: passedInYear
    })
  }
  
  render() {
    // console.log("Count: " + this.state.count1);

    // function setFullDate(dayOfMonth, month, year) {
    //   let clickedFullDate = dayOfMonth.toString() + month.toString() + year.toString();
    //   this.setState({
    //     fullDate: clickedFullDate,
    //     day: dayOfMonth,
    //     month: month,
    //     year: year
    //   })
    // }
    // console.log("Day-Render: " + this.state.day);
    let currentDayClicked = this.state.day;
    let currentMonthSet = this.state.month;
    let currentYearSet = this.state.year;
    console.log("App - Month: " + currentMonthSet);
    console.log("App - Year: " + currentYearSet);

    return (
    <div className="container display-flex">
      <div className="header display-flex">
        < Menu />
      </div>
      <div className="content_container display-flex">
        <div className="scroll-wrapper">
          <div className="grid-content-wrapper">
            <div className="calendar-container content1 display-grid">
              <Month 
                // setCurrentDay={this.state.setCurrentDay}
                setCurrentDay= 
                { (passedInDay) => this.setState({day: passedInDay}) }
                setCurrentMonth= {
                (passedInMonth) => this.setState({month: passedInMonth})}
                setCurrentYear=
                { (passedInYear) => this.setState({year: passedInYear})}
              />
            </div>
            <EventsList  
              currentDay={currentDayClicked} 
              month={currentMonthSet} 
              year={currentYearSet}
              count = {this.state.count1} />
          </div>
        </div>
      </div>
      <div className="footer display-flex">
        <div className="user-icon display-flex"><img src={icon} alt="icon"></img></div>
      </div>
    </div>
    )
  }
};

export default App;
