import React, { Component } from 'react'
import Day from './Day';

class DaysGrid extends Component {
  constructor(props) {
    super(props);
    // console.log("Props-DaysGrid: " + props.count1);
    // console.log("Props-Function-DaysGrid: " + props.setCurrentDay);
  }

  render() {
    let date = new Date();
    // Uses props from Month to determine currently selected month days to display
    let month = this.props.currentMonth;
    let year = date.getFullYear();
    let firstDay = new Date( date.getFullYear(), month, 1 ) ;
    let firstDayOfMonth = firstDay.getDay();
    // console.log(month);
    // *** USE CURRENT DAY TO HIGHLIGHT TODAY'S DATE
    // CODE STILL NEEDS TO BE WRITTEN FOR THIS
    // let currentDay = date.getDay();
    // console.log("Current Day: " + currentDay); 

  // ** BEGIN - CODE USED TO GENERATE CALENDAR DAYS

    // returns the number of days in month selected by user (uses month props from Month component)
    function daysInCurrentMonth() {
      return new Date(date.getFullYear(), month + 1, 0).getDate();
    }  

    // array used to populate calendar with correct # of days in current month.
    var arrPlaceholder = [];
    var arrDays = [];

    // addDivPlaceholders() task details:
    // If date is not Sunday, then the function pushes to array the
    // number of divs that are needed to prevent CSS Grid from
    // autofilling into previous calendar week spaces.
    // Also eliminates the need for previous switch function and the extra 
    // css class, "day-#"
    // Once populated with DIVs, the array will then be populated with
    // DIVs containing the correct number of days of the month.

    // function used to create filler DIVs before creating DIV for actual start of the month (details above)
    function addDivPlaceholders() {
      if (firstDayOfMonth !== 0 ) {
        let x = 100;
        for (var i = 1; i <= firstDayOfMonth; i++) {
          // to meet key-array requirements, assigning random set of numbers to 
          // beginning set of placeholder div's.
          x++;
          arrPlaceholder.push(<div key={x}></div>)
        }
      }
    }

    // *** ARRAY UNIQUE KEY ERROR - troubleshoot
    // function used to populate calendar with days of specified month
    function generateCurrentDaysOfMonth() {
      const numDaysInMonth = daysInCurrentMonth();
      // console.log(numDaysInMonth);
      // using Day component, loop adds onto array, generating actual set of calendar days
      for (var i = 1; i <= numDaysInMonth; i++) {
        arrDays.push(i);
      };
    };
    
    // ** START: CREATE CALENDAR DAYS
    addDivPlaceholders();
    generateCurrentDaysOfMonth();

    // stores array into variable to render day DIVs to DOM
    // var renderedOutput = (
    //   arr.map(day => <React.Fragment> {day} </React.Fragment>)
    // );
    // console.log(renderedOutput);

    function createCalendarDate() {
      let fullDate = month.toString() + year.toString();
      return fullDate;
    }
   
    return (
        <React.Fragment > 
          {/* placeholder array may need to have key defined in map function - check */}
          {arrPlaceholder.map(emptyDiv => emptyDiv)}
          {arrDays.map(day => 
          <Day 
            key={day.toString()} 
            dayOfMonth={day} 
            setCurrentDay={this.props.setCurrentDay}
            // count1={this.props.count1}
            // increaseCount={this.props.increaseCount}
          />)}
        </React.Fragment>
    )
  }
}

export default DaysGrid;