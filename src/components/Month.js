import React, { Component } from 'react';
import DaysGrid from './DaysGrid.js';

import left_arrow from './round_arrow_back_ios_white_48dp.png';
import right_arrow from './round_arrow_forward_ios_white_48dp.png';


class Month extends Component {
  constructor(props) {
    super(props);
    // console.log("Props-Month: " + props.count1);
    // console.log("Props-Function-Month: " + props.setCurrentDay);
    // Must initalize state first
    // count and index needed to ensure January is shown on screen (is this true??)
    this.state = {
      index : 0,
      count : 0,
      months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ],
      year : 2018
    };
    this.handleForwardClick = this.handleForwardClick.bind(this);
    this.handleBackClick = this.handleBackClick.bind(this);
  } // END CONSTRUCTOR

  // src. ref. https://daveceddia.com/visual-guide-to-state-in-react/

  // BEGIN HANDLEFORWARDCLICK()
  handleForwardClick() {
    // console.log("Count before: " + this.state.count);
    // console.log("Index before: " + this.state.index);
    // Increment index when button is clicked
      this.setState({
        index : (this.state.count++)
      }, function() {
        // setState is asynchronous! This function gets called
        // when it's finished.
        console.log("Month - function: " + this.state.count)
      });
  } // END HANDLEFORWARDCLICK()

  // BEGIN HANDLEBACKCLICK()
  handleBackClick() {
    // Decrement index when button is clicked
      // console.log("Count before: " + this.state.count)
      // console.log("Index before: " + this.state.index)
      this.setState({
        index : this.state.count--
      }, function() {
        // setState is asynchronous! This function gets called
        // when it's finished.
        // console.log("Count after: " + this.state.count)
        // console.log("Index after: " + this.state.index)
      });
  } // END HANDLEBACKCLICK()

  render() {
    let currentMonth = this.state.months[this.state.count];
    console.log("Month - Render: " + currentMonth);
    let monthNumericForm = this.state.count;
    console.log("Month - numericForm: " + this.state.count);
    
    // TO BE DEVELOPED
    // How to hide arrow buttons when there are no more months left to click through

    // console.log("Count Prop: " + this.props.count1);

    return (
      <React.Fragment>
        <div className="arrow-left" onClick={() => {this.handleBackClick(); this.props.setCurrentMonth(this.state.count); this.props.setCurrentYear(this.state.year)}}><img src={left_arrow}></img></div>
        <div className="arrow-right" onClick={() => {this.handleForwardClick(); this.props.setCurrentMonth(monthNumericForm); this.props.setCurrentYear(this.state.year)}}><img src={right_arrow}></img></div>
        <div className="month">{currentMonth}</div>
        <div className="week SUN">SUN</div>
        <div className="week MON">MON</div>
        <div className="week TUE">TUE</div>
        <div className="week WED">WED</div>
        <div className="week THU">THU</div>
        <div className="week FRI">FRI</div>
        <div className="week SAT">SAT</div>
        < DaysGrid 
          currentMonth={monthNumericForm}
          setCurrentDay={this.props.setCurrentDay}
          count1={this.props.count1}
          increaseCount={(this.props.increaseCount)}
        />
      </React.Fragment>
    )
  }
}

export default Month;