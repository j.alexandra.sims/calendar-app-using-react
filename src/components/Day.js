
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Day extends React.Component {
  constructor(props) {
    super(props);
    // console.log("Props-Day: " + props.count1);
    // console.log("Props-Function-Day: " + props.setCurrentDay);
  }
  
  // SHOULD HAVE ACCESS TO THE COUNT & INCREASECOUNT FUNCTION PARAMETERS W/O
  // HAVING TO DO ANYTHING ELSE.
  render() {
    // function increaseCount() {
    //   alert('Alert!');
    // };
    // onClick pulls event from db, sends it to the eventslist, then to event
    // console.log("Day of Month: " + this.props.dayOfMonth);
    // *** Can I send props to EventsList without returning it?
    // i.e. function() {
    //    <EventsList {this.props.dayOfMonth} />
    // }
    
    // component used by DaysGrid to create individual calendar days

    // HANDLECLICK (SETSTATE) NEEDS TO INCLUDE A CALLBACK FUNCTION THAT PASSES
    // DATA BACK UP TO APP.JS THAT CAN THEN BE ACCESSED BY EVENTSLIST
    // Pass state of this function up to DaysGrid, then Month, then App, so App
    // can pass that state on to EventsList.
    // Old function data from practice working through logic:
    // onClick= { () => this.props.increaseCount(this.props.count1 + 1) }

    // onClick={() => 
    //   this.props.setFullDate(this.props.dayOfMonth, this.props.month, this.props.year)}
    let dayOfMonth = this.props.dayOfMonth;
    // console.log("DAYOFMONTH: " + dayOfMonth);
    return (
      <div 
        className = {"day"}
        onClick={() => this.props.setCurrentDay(dayOfMonth)}
      >
        {this.props.dayOfMonth} 
      </div>
    )
  }
}

Day.propTypes = {
  dayOfMonth: PropTypes.number,
  month: PropTypes.number,
  year: PropTypes.number
}

export default Day;