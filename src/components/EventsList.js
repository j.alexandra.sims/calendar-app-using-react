// Manages state for all event items
// Currently working on integrating this logic with Firebase's
// Realtime Database API

import React, { Component } from 'react'
import PropTypes from 'prop-types';
// import firebase from 'firebase';
import Event from './Event.js';

class Events extends Component {
  constructor(props) {
    super(props);
    // console.log("Moment of Truth: " + props.day);
    // console.log("Actual Moment of Truth: " + props.currentDay);
    this.state = {
      events: [
        {
          id: 1,
          title: "Morning Meeting with Team",
          date: "2019-01-10",
          time: "9:00",
          location: "Austin, TX"
        },
        {
          id: 2,
          title: "Morning Meeting with New Client",
          date: "2019-01-10",
          time: "10:30",
          location: "Austin, TX"
        },
        {
          id: 3,
          title: "Lunch Meeting",
          date: "2019-01-10",
          time: "11:30",
          location: "Austin, TX"
        },
        {
          id: 4,
          title: "Coffee ",
          date: "2019-01-10",
          time: "14:00",
          location: "Austin, TX"
        },
        {
          id: 5,
          title: "Phone Call with Client",
          date: "2019-01-10",
          time: "15:30",
          location: "Austin, TX"
        },
        {
          id: 6,
          title: "JavaScript Meetup",
          date: "2019-01-10",
          time: "18:00",
          location: "Austin, TX"
        }
      ]
    }
  }

  // SUCCESS!! 
  componentDidUpdate() {
    let newCurrentDay = this.props.currentDay;
    console.log("EventsList componentDidUpdate- day! " + newCurrentDay);
    let newCurrentMonth = this.props.month;
    console.log("EventsList componentDidUpdate- month! " + newCurrentMonth);
    let newCurrentYear = this.props.year;
    console.log("EventsList componentDidUpdate- year! " + newCurrentYear);

    // CREATE FUNCTION FOR CONNNECTING WITH FIREBASE AND OUTPUTTING NEW ARRAY OF EVENTS
    // If day is not equal to zero, run firebase updateEvents function
    // *** INFO ON SETTING UP CONNECTION TO API:
    // https://engineering.musefind.com/react-lifecycle-methods-how-and-when-to-use-them-2111a1b692b1
    // HIGHLIGHTS: 
    // The exception is any setup that can only be done at runtime — namely, connecting to external API’s.
    // For example, if you use Firebase for your app, you’ll need to get that set up as your app is first mounting.
  }
  
  render() {
    const {events} = this.state;
    // console.log("State: " + this.state.events);
    
    console.log("EventsList render- day! " + this.props.currentDay);
    console.log("EventsList render- month! " + this.props.currentMonth);
    console.log("EventsList render- year! " + this.props.currentYear);

    //*** IF DAY/MONTH/YEAR = 0, DISPLAY NOTHING IN EVENTS, ELSE... DISPLAY

    return (
        <div className="events-container content2">
            { events.map(event => (
                // What we want to render for each contact.
                < Event 
                  key={event.id.toString()} // Must keep key stated for DB purposes.
                  event={event}
                />
              ))}
        </div> 
    )
  }
}

Event.propTypes = {
  event: PropTypes.object.isRequired,
}

export default Events;