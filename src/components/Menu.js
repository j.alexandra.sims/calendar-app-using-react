
import React, { Component } from 'react'

 class Menu extends Component {
  constructor(props) {
    super(props);

    this.state = {addClass: false};
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({addClass: !this.state.addClass});
  }

  render() {
    let menuClass=["menu-icon"];
    if(this.state.addClass) {
      menuClass.push('active');
    }
    return (
      <div className="menu-wrapper">
        <div className={menuClass.join(' ')} onClick={this.toggle}>
            <span></span>
            <span></span>
            <span></span>
        </div>
        {/* <span onclick="openNav()">&#9776;</span> */}
      </div>
    )
  }
}

export default Menu;