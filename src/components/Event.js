import React, { Component } from 'react'

class Event extends Component {
  render() {
    const { title, date, time, location } = this.props.event;
  
    return (
          <div className="list-event display-grid">
            <div className="event-title">{title}</div>
            <div className="event-location">{location}</div>
            <div className="event-date">{date}</div>
            <div className="event-time">{time}</div>
          </div>
    )
  }
}

export default Event;